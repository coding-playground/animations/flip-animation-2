/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _flip = __webpack_require__(1);

var _flip2 = _interopRequireDefault(_flip);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var flip2 = new _flip2.default();

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Flip2 = function () {
    function Flip2() {
        _classCallCheck(this, Flip2);

        this.imageMargin = 0;
        this.elementArray = [];
        this.rowArray = [];
        this.oldStates = [];
        this.newStates = [];
        this.initialize();
        this.setListeners();
    }

    _createClass(Flip2, [{
        key: 'initialize',
        value: function initialize() {
            var _this = this;

            var images = document.querySelectorAll('.gallery__image');
            var rows = document.querySelectorAll('.gallery__row');

            var rowWidth = 0;

            var elementStyle = window.getComputedStyle(images[0]);
            this.imageMargin = parseInt(elementStyle.marginRight, 10);

            images.forEach(function (image, index) {
                _this.elementArray.push(image);
                var imageWidth = image.getBoundingClientRect().width + _this.imageMargin;
                image.setAttribute('data-width', imageWidth);
                rowWidth += imageWidth;
            });
            rows.forEach(function (row, index) {
                _this.rowArray.push(row);
            });
            this.rowArray[0].removeAttribute('style');
            this.rowArray[0].style.width = rowWidth + 'px';
        }
    }, {
        key: 'setListeners',
        value: function setListeners() {
            var _this2 = this;

            var buttons = document.querySelectorAll('.button');

            buttons.forEach(function (button) {
                button.addEventListener('click', _this2.buttonClicked.bind(_this2));
            });
        }
    }, {
        key: 'buttonClicked',
        value: function buttonClicked(event) {
            var _this3 = this;

            this.oldStates = this.getElementPositions();

            var newRows = event.target.getAttribute('data-rows');
            var rowElements = Math.floor(this.elementArray.length / newRows);
            var rest = this.elementArray.length % newRows;

            var elementIndex = 0;
            for (var i = 0; i < newRows; i += 1) {
                var currentRowElements = rowElements;
                var rowWidth = 0;

                if (rest > 0) {
                    rest -= 1;
                    currentRowElements += 1;
                }

                for (var j = elementIndex, endIndex = elementIndex + currentRowElements; j < endIndex; j += 1) {
                    var image = this.elementArray[elementIndex];

                    this.rowArray[i].append(image);
                    rowWidth += image.getAttribute('data-width');
                    elementIndex += 1;
                }

                this.rowArray[i].style.width = rowWidth + 'px';
            }

            this.newStates = this.getElementPositions();

            // calculate inverse
            this.elementArray.forEach(function (element, index) {
                element.classList.remove('animating__flip');

                var invertedTop = _this3.oldStates[index].top - _this3.newStates[index].top;
                var invertedLeft = _this3.oldStates[index].left - _this3.newStates[index].left;

                element.style.transform = 'translate(' + invertedLeft + 'px, ' + invertedTop + 'px)';
            });

            var animationTimeout = setTimeout(function () {
                _this3.elementArray.forEach(function (element, index) {
                    element.classList.add('is-animating');
                    element.style.transform = '';
                    element.addEventListener('transitionend', Flip2.animationCallback);
                });
            }, 10);
        }
    }, {
        key: 'getElementPositions',
        value: function getElementPositions() {
            var retrievedStates = [];

            this.elementArray.forEach(function (listItem) {
                retrievedStates.push(listItem.getBoundingClientRect());
            });

            return retrievedStates;
        }
    }], [{
        key: 'animationCallback',
        value: function animationCallback(event) {
            var target = event.target;

            target.classList.remove('is-animating');
            if (target.style.length === 0) {
                target.removeAttribute('style');
            }
            target.removeEventListener('transitionend', Flip2.animationCallback);
        }
    }]);

    return Flip2;
}();

exports.default = Flip2;

/***/ })
/******/ ]);