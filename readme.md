# FLIP animations 2

## Description

This way of optimizing animation performance was described by Paul Lewis on [https://aerotwist.com](https://aerotwist.com).

FLIP stands for First, Last, Invert, Play.

The process of animating an element is split up into different parts. Putting all calculations in a small time frame of about 100ms after the users interaction.
During that time he wouldn't notice a short break in the response of the application.

We will use this time to do all the heavy calculations and in the end the browser just needs to tween the values.

This is achieved by breaking down the process into 5 steps:

1. FIRST: Retrieve position and dimensions before making any changes. The function element.getBoundingClientRect() can be used for that.

2. Set the new status of your element. Either by adding a class that applies changes or be actually putting it to another place in the DOM.

3. LAST: Retrieve position and dimensions after the changes the way you did before.

4. INVERT: For each value that should be animated calculate the inverse and apply it as a transform.
    For example: const invert = first.top - last.top;

    This value is applied to the element as a transform: element.style.transform = \`translateY(${invert}px)\`;

    Now the Element looks like nothing happened and all calculations that might lead to a non responding app later are already done.
   
5. PLAY: Now it is time to start the animation. Request an animation frame, add a css-class that enables transitions and after that remove the transform you added earlier: element.style.transform = ''';

    After the animation finished playing remove that css-class again.
   
Now one might ask why creating such a complex process if the element could simply be animated with the help of jQuery.animate() or by adding CSS transitions.

At the first look the calculations do not seem to be that complicated. But in fact by just tweening let's say a top-value the browser has to repaint all parts of the DOM that are influenced b the elements position. And that not just once but 60 times a second.

Using a transform the element itself is put into another painting context. The browser can now easily tween the the position 'back' to the final position without having to calculate any other positions.

It should be pointed out that to use this technique, it is mandatory to animate only compositor-friendly transform and opacity changes.

## Experiment 2

In this sample I wanted to animate the position of images in a gallery with multiple rows.
The animation occurs when switching the amount of rows.

This effect might be known from Javascript plugins like masonry.

Like in the first example I had to replace the requestAnimationFrame method with a short timeout of 10ms.


## For further reading:

[https://aerotwist.com/blog/flip-your-animations/](https://aerotwist.com/blog/flip-your-animations/)


