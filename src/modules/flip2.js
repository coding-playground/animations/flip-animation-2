class Flip2 {
    constructor() {
        this.imageMargin = 0;
        this.elementArray = [];
        this.rowArray = [];
        this.oldStates = [];
        this.newStates = [];
        this.initialize();
        this.setListeners();
    }

    initialize() {
        const images = document.querySelectorAll('.gallery__image');
        const rows = document.querySelectorAll('.gallery__row');

        let rowWidth = 0;

        const elementStyle = window.getComputedStyle(images[0]);
        this.imageMargin = parseInt(elementStyle.marginRight, 10);

        images.forEach((image, index) => {
            this.elementArray.push(image);
            const imageWidth = image.getBoundingClientRect().width + this.imageMargin;
            image.setAttribute('data-width', imageWidth);
            rowWidth += imageWidth;
        });
        rows.forEach((row, index) => {
            this.rowArray.push(row);
        });
        this.rowArray[0].removeAttribute('style');
        this.rowArray[0].style.width = `${rowWidth}px`;
    }

    setListeners() {
        const buttons = document.querySelectorAll('.button');

        buttons.forEach((button) => {
            button.addEventListener('click', this.buttonClicked.bind(this));
        });
    }

    buttonClicked(event) {
        this.oldStates = this.getElementPositions();

        const newRows = event.target.getAttribute('data-rows');
        const rowElements = Math.floor(this.elementArray.length / newRows);
        let rest = this.elementArray.length % newRows;

        let elementIndex = 0;
        for (let i = 0; i < newRows; i += 1) {
            let currentRowElements = rowElements;
            let rowWidth = 0;

            if (rest > 0) {
                rest -= 1;
                currentRowElements += 1;
            }

            for (let j = elementIndex, endIndex = elementIndex + currentRowElements; j < endIndex; j += 1) {
                const image = this.elementArray[elementIndex];

                this.rowArray[i].append(image);
                rowWidth += image.getAttribute('data-width');
                elementIndex += 1;
            }

            this.rowArray[i].style.width = `${rowWidth}px`;
        }

        this.newStates = this.getElementPositions();


        // calculate inverse
        this.elementArray.forEach((element, index) => {
            element.classList.remove('animating__flip');

            const invertedTop = this.oldStates[index].top - this.newStates[index].top;
            const invertedLeft = this.oldStates[index].left - this.newStates[index].left;

            element.style.transform = `translate(${invertedLeft}px, ${invertedTop}px)`;
        });

        const animationTimeout = setTimeout(() => {
            this.elementArray.forEach((element, index) => {
                element.classList.add('is-animating');
                element.style.transform = '';
                element.addEventListener('transitionend', Flip2.animationCallback);
            });
        }, 10);
    }

    getElementPositions() {
        const retrievedStates = [];

        this.elementArray.forEach((listItem) => {
            retrievedStates.push(listItem.getBoundingClientRect());
        });

        return retrievedStates;
    }

    static animationCallback(event) {
        const target = event.target;

        target.classList.remove('is-animating');
        if (target.style.length === 0) {
            target.removeAttribute('style');
        }
        target.removeEventListener('transitionend', Flip2.animationCallback);
    }
}

export default Flip2;
